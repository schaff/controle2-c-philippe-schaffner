#include <iostream>
#include <stdlib.h>
#include <time.h>

using namespace std;

struct MastermindCombinaison
{
    int nombreEssai=10;
    int nombrePartiGagne=0;
    int nombrePartiPerdu=0;
    int tabCombinaison[4];
};

void afficheChoix (int choixUn, int choixDeux, int choixTrois, int choixQuatre) // AFFICHAGE DES CHOIX DU JOUEUR
{
    cout<<"........."<<endl;
    cout<<"|"<<choixUn<<"|"<<choixDeux<<"|"<<choixTrois<<"|"<<choixQuatre<<"|"<<endl;
    cout<<"........."<<endl;
}

int controleChoix(int choix) // CONTROLE DU CHOIX DU JOUEUR !
{
    while (choix <0  || choix >9)
    {
        cout<<"ADRIEN IL NE FAUT PAS ESSAYER DE CASSER MON CODE ! RENTRE UN CHIFFRE ENTRE 0 ET 9 STP!"<<endl;
        cin>>choix;
    }
    return (choix);
}

int main()
{
// INITIALISATION DES VARIABLES
    int sortieBoucle=0;
    int tabChoix[4];
    int finJeu=0;
    int malPlace=0;
    int bienPlace=0;
    MastermindCombinaison jeu;
    srand(time(NULL));
// FINI INITIALISATION VARIABLES


// INITIALISATION DU TABLEAU A 0
    for (int i=0; i<4; ++i)
    {
        jeu.tabCombinaison[i]=0;
        tabChoix[i]=0;
    }
// FIN INITIALISATION TABLEAU


// BOUCLE POUR CHOISIR SI ON VEUT REJOUER OU QUITTER
    while (sortieBoucle==0)
    {
        cout<<"BONJOUR, VOUS ALLEZ JOUER AU MASTERMIND !"<<endl<<"VOUS N'ETES PAS PRET !"<<endl<<endl;

        jeu.nombreEssai=10;
        finJeu=0;

        for (int i=0; i<4; ++i) // BOUCLE INITIALISATION DU TABLEAU AVEC LE RAND
        {
            jeu.tabCombinaison[i]=rand()%10;
        }


    // BOUCLE D UNE PARTIE
        while (jeu.nombreEssai>=0 && finJeu==0)
        {
            --jeu.nombreEssai;

            for (int i=0; i<4; ++i) // BOUCLE POUR CHOISIR LES CHIFFRES QU ON JOUE
            {
                cout<<"METTRE EN PLACE LE CHIFFRE "<<i<<", IL EST ENTRE 0 ET 9"<<endl;
                cin>>tabChoix[i];
                tabChoix[i]=controleChoix(tabChoix[i]);
            }

            afficheChoix(tabChoix[0], tabChoix[1], tabChoix[2], tabChoix[3]); // affiche un tableau pour visualiser les choix qu'on fait

            bienPlace=0;
            malPlace=0;

            for (int i=0; i<4; ++i) // BOUCLE DE TEST
            {
                if (jeu.tabCombinaison[i]==tabChoix[i]) // v�rif si bien plac�
                {
                    ++bienPlace;
                }
                else
                {
                   for (int j=0; j<4; ++j)
                    {
                        if (jeu.tabCombinaison[i]==tabChoix[j]) // v�rif s'il existe dans la combinaison
                        {
                            ++malPlace;
                        }
                    }
                }

            }

            if (bienPlace == 4) // condition pour sortir de la boucle en gagnant !
            {
                finJeu=1;
                ++jeu.nombrePartiGagne;
            }
            cout<<"CHIFFRE MAL PLACE : "<<malPlace<<endl<<"CHIFFRE BIEN PLACE : "<<bienPlace<<endl<<"NOMBRE D ESSAI RESTANT : "<<jeu.nombreEssai<<endl<<endl;
        }
    // FIN BOUCLE PARTIE
        if (jeu.nombreEssai==0) // v�rification si la partie est gagn� ou perdu !
        {
            ++jeu.nombrePartiPerdu;
        }
        cout<<"TU AS GAGNER "<<jeu.nombrePartiGagne<<" PARTI"<<endl;
        cout<<"TU AS PERDU "<<jeu.nombrePartiPerdu<<" PARTI"<<endl;
        cout<<"GAME OVER ! "<<endl<<"RETRY (0) / QUIT (1)"<<endl; // demande au joueur s'il continuer ou non
        cin>>sortieBoucle;
        cout<<endl;
    }
// FIN BOUCLE JEU

    return 0;
}

